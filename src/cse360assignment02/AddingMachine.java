package cse360assignment02;
public class AddingMachine {
    
  private int total;
  private String history;
  
  public AddingMachine () {
    total = 0;  // not needed - included for clarity
    history = " 0 ";
  }
  
  /*
  returns total
  */
  public int getTotal () {
    return total;   
  }
  
  /*
  Adds
  */
  public void add (int value) {
      total += value;
      history = history + " + " + value;
  }
  /*
  subtracts
  */
  public void subtract (int value) {
      total -= value;
      history = history + " - " + value;
  }
  
  /*
  returns history string
  */
  public String toString () {
    return history;
  }

  /*
  clears operations
  */
  public void clear() {
      total = 0;
      history = "0";  
  }
}